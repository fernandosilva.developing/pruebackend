from model import Movie
import motor.motor_asyncio


#for the connection between database.py and mongodb:
client = motor.motor_asyncio.AsyncIOMotorClient('mongodb://localhost:27017')


database = client.movies

# collection = table
collection = database.movie

'''
{
  "poster_path": "/IfB9hy4JH1eH6HEfIgIGORXi5h.jpg",
  "adult": false,
  "overview": "Jack Reacher must uncover the truth behind a major government conspiracy in order to clear his name. On the run as a fugitive from the law, Reacher uncovers a potential secret from his past that could change his life forever.",
  "release_date": "2016-10-19",
  "genre_ids": [
    53,
    28,
    80,
    18,
    9648
  ],
  "id": 343611,
  "original_title": "Jack Reacher: Never Go Back",
  "original_language": "en",
  "title": "Jack Reacher: Never Go Back",
  "backdrop_path": "/4ynQYtSEuU5hyipcGkfD6ncwtwz.jpg",
  "popularity": 26.818468,
  "vote_count": 201,
  "video": false,
  "vote_average": 4.19

'''


# recuperacion
async def fetch_one_movie(id):
    #document is a row in sql
    document = await collection.find_one({"id":id})
    return document


async def fetch_all_movies():
    movies = []
    cursor = collection.find({}) #loop over any document in cursor (all collection documents)
    async for document in cursor:
        movies.append(Movie(**document))

    return movies

async def create_movie(movie):
    document = movie
    result = await collection.insert_one(document)
    return document

async def update_movie(id, poster_path, adult, overview,release_date,genre_ids,original_title,original_language,backdrop_path,popularity,vote_count,video,vote_average):
    await collection.update_one(
        {'id':id}, 
        {'$set':
            {'poster_path':poster_path,
            'adult':adult,
            'overview':overview,
            'release_date':release_date,
            'genre_ids':genre_ids,
            'original_title':original_title,
            'original_language':original_language,
            'backdrop_path':backdrop_path,
            'popularity':popularity,
            'vote_count':vote_count,
            'video':video,
            'vote_average':vote_average},

        })
    document = await collection.find_one({"id":id})


async def remove_movie(id):
    await collection.delete_one({"id": title})
    return True

