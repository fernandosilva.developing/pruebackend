from fastapi import FastAPI, HTTPException
from fastapi.middleware.cors import CORSMiddleware
from database import fetch_one_movie, fetch_all_movies,create_movie, update_movie, remove_movie 
from server.model import Movie
#router object

router = APIRouter()


#This for CORS
origins = ['https://localhost:3000']

router.add_middleware(
    CORSMiddleware,
    allow_origins = origins,
    allow_credentials = True,
    allow_methods = ["*"],
    allow_headers = ["*"],
)

@router.get("/")
def read_root():
    return {"Hello":"xd"}


@router.get("/api/movie")
async def get_movie():
    response  = await fetch_all_movies()
    return response


@router.get("/api/movie{id}", response_model = Movie)
async def get_movie_by_id(id):
    response = await fetch_one_movie(id)
    if response:
        return response
    raise HTTPException(404, f"There is no movie item with this id: {id}")


@router.post("/api/movie", response_model = Movie)
async def post_movie(movie: Movie):

    response = await create_movie(movie.dict())
    if response:
        return response
    raise HTTPException(400, "Something went wrong")


@router.put("/api/movie{id}/", response_model = Movie)
async def edit_movie(id:int, poster_path:str,
    adult:bool,
    overview:str,
    release_date:str,    
    genre_ids:list,
    original_title:str,
    original_language:str,
    backdrop_path:str,
    popularity:float,
    vote_count:int,
    video:bool,
    vote_average:float):
    response = await update_movie(id,  poster_path, adult, overview,release_date,genre_ids,original_title,original_language,backdrop_path,popularity,vote_count,video,vote_average)
    if response:
        return response 
    raise HTTPException(404, f"There is no movie item with this id {id}")

@router.delete("/api/movie{id}")
async def delete_movie(id):
    response = await remove_movie(id)
    if response:
        return "Succesfully deleted movie item"
    raise HTTPException(404, f"there is no movie item with this id {id}")
